/*
  * # Aws Ecs cluster
  * 
  * ## Description
  * 
  * This module create :
  * - ECS cluster
  * - Security group for ecs
  * - IAM roles + roles policies
  * 
*/
locals {
  // Do not add ${var.namespace} in vpc name because it will be added by the vpc module
  ecs_instance_iam_full_role_name   = "${var.namespace}-${var.stage}-ecsInstanceRole"
  iam_full_task_execution_role_name = "${var.namespace}-${var.stage}-ecsTaskExecutionRole"
  ecs_full_cluster_name             = "${var.namespace}-${var.stage}-${var.name}"
  ecs_security_group_name           = "${var.namespace}-${var.stage}-ecs"
  ecs_cloudwatch_full_policy_name   = "${var.namespace}-${var.stage}-ecsCloudWatchLogs"

  cloudwatch_full_prefix_name    = "${var.namespace}-${var.stage}-ecs"
  cloudwatch_log_group_dmesg     = "/var/log/dmesg"
  cloudwatch_log_group_ecs_agent = "/var/log/ecs/ecs-agent.log"
  cloudwatch_log_group_ecs_init  = "/var/log/ecs/ecs-init.log"
  cloudwatch_log_group_audit     = "/var/log/ecs/audit.log"
  cloudwatch_log_group_messages  = "/var/log/messages"

  cloudwatch_config = templatefile("${path.module}/templates/logs.json", {
    cloudwatch_full_prefix_name = local.cloudwatch_full_prefix_name
    dmesg                       = local.cloudwatch_log_group_dmesg
    ecs_agent                   = local.cloudwatch_log_group_ecs_agent
    ecs_init                    = local.cloudwatch_log_group_ecs_init
    audit                       = local.cloudwatch_log_group_audit
    messages                    = local.cloudwatch_log_group_messages
  })
}
